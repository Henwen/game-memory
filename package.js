    // -------- Object -------- //
    /**
     * 圖片類別
     * @param img_value 'number': 比對圖片是否相同的數值
     * @param img_is_disabled 'boolean': 圖片是否可點擊
     * @param img_content 'string': 圖片內容字串 e.g. 1, 3, 5, ..., 27
     */
    function Picture(img_value, img_is_disabled, img_content) {
        this.value    = img_value ;
        this.disabled = img_is_disabled ;
        this.content  = img_content ;
    }

    /**
     * 圖片鎖定功能
     * @param flag 'boolean': 圖片鎖定參數 true:鎖定 | false:不鎖定
     */
    Picture.prototype.setImageDisabled = function (flag) {
        this.disabled = typeof flag === "boolean" ? flag : true ;
    }

    /**
     * 圖片鎖定狀態
     */
    Picture.prototype.getImageDisabledStatus = function () {
        return this.disabled ;
    }

    /**
     * 設定圖片為 ?，並解開圖片鎖定
     * @param img_dom 'object': 該圖片所屬 span 物件
     */
    Picture.prototype.closeImage = function (img_dom) {
        img_dom.innerHTML = "?" ;
        img_dom.className = "" ;
        this.setImageDisabled(false) ;
    }

    /**
     * 翻開圖片內容
     * @param img_dom 'object': 該圖片所屬 span 物件
     */
    Picture.prototype.openImage = function (img_dom) {
        img_dom.innerHTML = this.content ;
        img_dom.className = "text" ;
    }

    // -------- Array Prototype -------- //

    // Prototype: 陣列的洗牌函式
    Array.prototype.shuffle = function () {
        var o = this ;
        for (var j, x, i = o.length; i ; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x) ;
        return o ;
    }
    // -------- Array Prototype -------- //

    /**
     * 計數器產生器
     * @param show_id 'string': 要顯示在哪個 Dom id
     */
    var counter_maker = function (show_id) {
        // 累計次數
        var count = 0 ;
        var id = show_id ;
        return {
            // 取出累計次數
            getCount: function () {
                return count ;
            },
            // 累加次數
            increment: function () {
                count += 1 ;
                this.setCount() ;
            },
            // 顯示次數
            setCount: function () {
                document.getElementById(id).innerHTML = this.getCount() ;
            }
         };
    } ;
