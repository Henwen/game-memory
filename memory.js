	/**
	 * 遊戲控制變數
	 * @param game_size 'number': 遊戲尺寸, 偶數 e.g. 4
	 * @param game_pair_size 'number': 遊戲圖片配對張數, 是遊戲尺寸的因數, e.g. 2, 3, 4
	 * @param counter_object 'object': 倒數器
	 */
	var game_controller = function (game_size, game_pair_size, counter_object, table_id) {
		// 遊戲尺寸必須為整數
		game_size = Math.floor(game_size) ;
		game_pair_size = Math.floor(game_pair_size) ;

		// 檢驗遊戲尺寸
		try {
			if ( typeof game_size !== 'number') {
				throw {
					"name": "c001",
					"message": "遊戲尺寸必須為 number 型態"
				} ;
			}

			if (game_size <= 0 || game_pair_size <= 0 || Math.pow(game_size, 2) % game_pair_size !== 0) {
				throw {
					"name": "c002",
					"message": "遊戲尺寸不符合規定"
				} ;
			}
		} catch (e) {
			alert(e.message) ;
			return false ;
		}

		return {
			size: game_size,		// 遊戲尺寸
			pair_size: game_pair_size, // 遊戲配對張數
			has_opened: 0,			// 目前打開的圖片數量
			game_disabled: false,	// 遊戲圖片是否能被點選
			pair_find: 0,			// 紀錄成功打開配對數
			images_name: [], 		// 遊戲圖片檔名
			images: [],				// 遊戲圖片物件陣列
			click_img_dom: [],		// 被點擊的圖片 Dom
			open_img_object: [],	// 被打開的圖片物件
			open_second: 1,			// 兩張圖片比對錯誤時的停留秒數
			counter: counter_object,// 點擊次數計算物件
			table: table_id,		// 在哪個表單 Dom id 下進行遊戲

			// 遊戲圖片實體初始化
			initialImages: function () {
				// 計算需要多少張
				var images_total = Math.pow(this.size, 2) / this.pair_size ;

				// 初始化圖片內容陣列: [1, 2, ..., N]
				this.images_name = new Array(images_total) ;
				for (var i = 0 ; i < this.images_name.length ; i++) {
					this.images_name[i] = i+1 ;
				}

				// 隨機排序
				this.images_name.shuffle() ;

				// 初始化圖片物件
				for (var i = 0 ; i < images_total ; i++) {
					// 同一張圖片產生多個圖片物件, 依據配對張數 pair_size
					for (var j = 0 ; j < this.pair_size ; j++) {
						this.images[ i * this.pair_size + j ] = new Picture( i + 1, false, this.images_name[i] ) ;
					}
				}

				// 將圖片陣列亂數排序
				this.images.shuffle() ;
			},

			// 設定遊戲圖片 Dom
			setGameTable: function () {
				var str = "" ;
				for (var i = 0 ; i < this.size ; i++) {
					str += "<tr>" ;
					for (var j = 0 ; j < this.size ; j++) {
						str += "<td><span id="+(i*this.size+j)+">?</span></td>" ;
					}
					str += "</tr>" ;
				}
				document.getElementById(this.table).innerHTML = str ;
			},

			// 設定圖片點擊事件
			setClickEvent: function () {
				// 閉包函式： 'this' assigned to 'that'
				var that = this ;

				// 取得指定 table 內的 span
				var table = document.getElementById(this.table) ;
				var tags = table.getElementsByTagName("span") ;
				for (var i = 0 ; i < tags.length ; i++) {
					tags[i].onclick = function () {
						that.handleClick(this) ;
					} ;
				}
			},

			/**
		     * 處理圖片點擊事件
		     * @param img_element 圖片 HTML Dom 元素
		     */
			handleClick: function (img_element) {
				// 取得圖片物件
				var img_object = this.images[img_element.id] ;

				// 判斷遊戲是否可繼續點擊動作
				if (! this.getGameDisabledStatus()) {

					// 立即鎖住遊戲點擊，避免使用者持續點擊行為。
					this.setGameDisabled(true) ;

					// 判斷單一圖片是否可點擊
					if ( ! img_object.getImageDisabledStatus() ) {
						console.log("被翻開的圖片是: " + img_object.content) ;
						// 有效點擊，次數增加。
						this.counter.increment() ;

						// 翻開圖片
						img_object.openImage(img_element) ;

						// 目前翻開第幾張圖片: 1 or 2
						this.has_opened += 1 ;

						// 將點擊的物件都放至容器中
						this.click_img_dom.push(img_element) ;
						this.open_img_object.push(img_object) ;

						// 鎖住被點擊的圖片
						img_object.setImageDisabled(true) ;

						// 翻開圖片的比對
						if (this.has_opened === this.pair_size) {
							// 如果圖片相同
							if ( this.sameImage() ) {
								// 檢驗是否完成
								this.success() ;
								// 遊戲解鎖
								this.setGameDisabled(false) ;
								// 清除容器內的點擊物件
								this.clearContainer() ;
							}
							else {
								// 讓圖片依序回復成問號圖片
								this.recoverImage() ;
							}
							// 計算開啟圖片數歸零
							this.has_opened = 0 ;
						}
						else {
							this.setGameDisabled(false) ;
						}
					} // End - if 單一圖片是否能點擊
					else {
						this.setGameDisabled(false) ;
					}
				} // End - if 檢驗遊戲是否能點擊
			}, // End - handleClick Event

			// 設定遊戲鎖定狀態
			setGameDisabled: function (flag) {
				this.disabled = typeof flag === "boolean" ? flag : true ;
			},

			// 遊戲鎖定狀態
			getGameDisabledStatus: function () {
				return this.disabled ;
			},

			// 檢驗點選的圖片是否相同
			sameImage: function () {
				var v1, v2 ;
				v1 = this.open_img_object[0].value ;
				for (var i = 1 ; i < this.open_img_object.length ; i++) {
					v2 = this.open_img_object[i].value ;
					if ( v1 !== v2) {
						return false ;
					}
					v1 = v2 ;
				}
				return true ;
			},

			// 復原成問號
			recoverImage: function () {
				for (var i = 0 ; i < this.pair_size ; i++) {
					this.recoverImageSetTime(i) ;
				}
			},

			// 定時恢復圖片
			recoverImageSetTime: function (i) {
				var that = this ;
				if (i === this.pair_size - 1) {
					setTimeout(function () { that.countDown (i, that.click_img_dom[i], that.open_img_object[i], true ) ; }, (this.open_second + (i*0.1)) * 1000) ;
				}
				else {
					setTimeout(function () { that.countDown (i, that.click_img_dom[i], that.open_img_object[i], false ) ; }, (this.open_second + (i*0.1)) * 1000) ;
				}
			},

			// 倒數計時，並將翻開的圖片恢復成問號圖片，再將遊戲解鎖。
			countDown: function (i, img_dom, img_object, flag) {
				img_object.closeImage(img_dom) ;
				// 遊戲解鎖
				if (flag) {
					this.setGameDisabled(false) ;
					this.clearContainer() ;
				}
			},

			// 檢驗成功
			success: function () {
				this.pair_find++ ;
				if ( this.pair_find === Math.pow(this.size, 2) / this.pair_size) {
					alert("恭喜過關") ;
					return false ;
				}
			},

			// 清除點擊物件
			clearContainer: function () {
				this.click_img_dom = [] ;
				this.open_img_object = [] ;
			},

		} ; // End - return
	} ;

	// 按鈕：建立遊戲
	var btn = document.getElementById("btn-create") ;
	btn.onclick = function () {

		// 取得遊戲尺寸
		var size = document.getElementById("game-size").value ;
		var pair_size = document.getElementById("game-pair-size").value ;

		// 初始化遊戲計算數器
		counter_object = counter_maker("count") ;

		// Game Start
		game_object = game_controller(parseInt(size), parseInt(pair_size), counter_object, "memory") ;

		//
		if (typeof game_object === "object") {
			game_object.initialImages() ;
			game_object.setGameTable() ;
			game_object.setClickEvent() ;
		}
	}
